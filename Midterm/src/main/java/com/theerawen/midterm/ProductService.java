/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theerawen.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class ProductService {
    private static ArrayList<Product> posList = new ArrayList<>();
    static{
        load();
    }
    //create 
    public static boolean addProduct(Product product){
        posList.add(product);
        save();
        return true;
    }//delete
    public static boolean delProduct(int index){
        posList.remove(index);
        save();
        return true;
    }
     //read
     public static ArrayList<Product> getProductall(){
         return posList;
     }
     public static Product getProduct(int index){
         return posList.get(index);
     }
     //update
     public static boolean updateProduct(int index,Product product){
         posList.set(index, product);
         save();
         return true;
     }
    public static void save(){
            File file = null;
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
        try {
            file = new File("wen.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(posList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    public static void load(){
            File file = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
        try {
            file = new File("wen.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
           posList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }    
        
    }
      
}
