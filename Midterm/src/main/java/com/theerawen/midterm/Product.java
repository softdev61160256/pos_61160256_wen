/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theerawen.midterm;

import java.io.Serializable;

/**
 *
 * @author Windows10
 */
public class Product implements Serializable{
    private int id;
    private String name;
    private String brand;
    private double price;
    private int amount;
    private double sum = 0;
    private int item = 0;
    
    
    public Product(int id,String name,String brand,double price,int amount){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public String getbrand() {
        return brand;
    }

    public double getPrice() {
        return price;
    }
    public int getAmount() {
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public void setSum(double sum){
        this.sum+=price;
    }
    public double getSum(){
        return sum;
    }
    public int getItem(){
        return item;
    }
    public void setItem(int item){
        item++;
    }

    @Override
    public String toString() {
        return "Product Id = "+ id + "  | Name = "+ name+"  | Brand = "+brand+
                "   | Price = "+price+" | Amount = "+amount;
    }
    
}
